require "lib.slam"
vector = require "lib.hump.vector"
tlfres = require "lib.tlfres"

require "helpers"

TILESIZE = 20
CATSIZE = 35
PLAYERSIZE = 35
ROOMBASIZE = 60
CANVAS_WIDTH = 1600
CANVAS_HEIGHT = 900

startInstructions = "Made for Ludum Dare 40 in 72 hours by blinry, dasMaichen, Flauschzelle, and winniehell\n(press return to play)"
introductionInstructions = "(press return to play)"
gameInstructions = "Arrow keys: move - Hold space and move away from a wall: draw a tape\nQ: remove tape - E: pick up/drop cat or turn roomba on/off - Esc: pause"
pauseInstructions = "(press return to continue playing - press ESC to restart game)"
winInstructions = "(press return to start next level)"
failInstructions = "(press return to restart level)"
endInstructions = "(press return to start again - press ESC to quit)"


function love.load()
    -- set up default drawing options

    love.graphics.setDefaultFilter( "nearest", "nearest", 1 )
    love.graphics.setBackgroundColor(0, 0, 0)

    instructions = startInstructions -- default value for instructions text

    -- load assets
    images = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("images")) do
        if filename ~= ".gitkeep" then
            images[filename:sub(1,-5)] = love.graphics.newImage("images/"..filename)
        end
    end

    sounds = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("sounds")) do
        if filename ~= ".gitkeep" then
            sounds[filename:sub(1,-5)] = love.audio.newSource("sounds/"..filename, "static")
        end
    end
    sounds.purr:setVolume(0.4)
    sounds.meow:setVolume(0.1)

    music = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("music")) do
        if filename ~= ".gitkeep" then
            music[filename:sub(1,-5)] = love.audio.newSource("music/"..filename, "stream")
            music[filename:sub(1,-5)]:setLooping(true)
        end
    end

    music.roomba:setVolume(0.2)
    music.cat_house:setVolume(0.5)
    soundtrack = music.waiting_cat:play()

    fonts = {}
    for i,filename in pairs(love.filesystem.getDirectoryItems("fonts")) do
        if filename ~= ".gitkeep" then
            fonts[filename:sub(1,-5)] = {}
            for fontsize=50,100 do
                fonts[filename:sub(1,-5)][fontsize] = love.graphics.newFont("fonts/"..filename, fontsize)
            end
        end
    end
    love.graphics.setFont(fonts.m5x7[50])

    -- set up levels
    -- max lvlheight should be 18, max lvlwidth should be 40!
    levels = {}
    levels[1] = {lvlwidth = 30, lvlheight = 15, numberOfCats = 1, music = music.cat_house, story = "I think my cat was a bit lonely, so I got a second one! <3 \nAnnnd I have to clean my room again."}
    levels[2] = {lvlwidth = 40, lvlheight = 12, numberOfCats = 2, music = music.cat_house, story = "My neighbors asked me to take care of their cats for a while. \nI'll gladly help, but my floor looks dusty again already..."}
    levels[3] = {lvlwidth = 30, lvlheight = 18, numberOfCats = 5, music = music.catter_house, story = "I feel sorry for those cats at the animal shelter... \nI wonder if I should maybe adopt, um, all of them?"}
    levels[4] = {lvlwidth = 40, lvlheight = 17, numberOfCats = 15, music = music.cattest_house, story = "Yesterday, I heard people talk about some \"Crazy Cat Lady\". I wonder who they mean?"}

    state = "title"
    level = 1
end

function loadLevel(lvl)
    -- stop roomba sound
    music.roomba:pause()

    -- get level number from input parameter
    level = lvl

    -- end game if the previous level was the last one
    if level > #levels then
	state = "end"
	text = "That seems like enough cleaning for this year!\n\nThank you for playing! <3"
	instructions = endInstructions
	return
    end

    -- set instructions for game status
    instructions = gameInstructions

    soundtrack:pause()
    soundtrack = levels[level].music:play()

    -- set up game objects

	-- set up player
    player = {x=(2*TILESIZE+(PLAYERSIZE/2)), y=(4*TILESIZE-(PLAYERSIZE/2)), vx=0, vy=0, image=images.child_down, holding=nil, taping=false, tapeStartX=nil, tapeStartY=nil, collide={{x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}, {x=0, y=0}}}

	-- set up map
    map = {}
    for x=1,levels[level].lvlwidth do
        map[x] = {}
        for y=1,levels[level].lvlheight do
            map[x][y] = {wall=false, clean=false, tape=false, wip=false}
        end
    end

    for x=1,levels[level].lvlwidth do
        map[x][1].wall = true
        map[x][levels[level].lvlheight].wall = true
    end

    for y=1,levels[level].lvlheight do
        map[1][y].wall = true
        map[levels[level].lvlwidth][y].wall = true
    end

    -- set up cats
    cats = {}
    for x=1,levels[level].numberOfCats do
        repeat
            cx = love.math.random(3*TILESIZE,(levels[level].lvlwidth-3)*TILESIZE)
            cy = love.math.random(3*TILESIZE,(levels[level].lvlheight-3)*TILESIZE)
        until not isSolid(cx, cy)
        vx = love.math.random(1,2) == 1 and -1 or 1
        vy = love.math.random(1,2) == 1 and -1 or 1
        -- define random colors
		-- old version (for love 10.x):
        --r = love.math.random(200, 255)
		--g = love.math.random(150, 255)
		--b = love.math.random(100, 255)

		-- colors for love versions 11.x
        r = love.math.random(200, 255) / 255
		g = love.math.random(150, 255) / 255
		b = love.math.random(100, 255) / 255

        table.insert(cats, {x=cx, y=cy, vx=vx, vy=vy, held=false, image=(vy == -1 and images.cat_up or images.cat_down), color = {r, g, b}})
    end

    -- set up roomba
    roomba = {x=(2*TILESIZE+(ROOMBASIZE/2)), y=((levels[level].lvlheight)*TILESIZE)-(ROOMBASIZE/2), vx=0, vy=-3, image=images.roomba_up, on=false, collide={{x=0, y=0},{x=0, y=0},{x=0, y=0}}}
end

function love.update(dt)
    if state == "game" then
        -- paint tapes with player
        if player.taping then
            mx, my = player.x, player.y
            x = math.floor(mx/TILESIZE)
            y = math.floor(my/TILESIZE)

            if not (map[x][y].wall or map[x][y].tape) then
                map[x][y].wip = true
            end

            if not (x == player.tapeStartX and y == player.tapeStartY) then
                if player.tapeDir == "up" and isSolidTile(x,y-1) or
                   player.tapeDir == "down" and isSolidTile(x,y+1) or
                   player.tapeDir == "left" and isSolidTile(x-1,y) or
                   player.tapeDir == "right" and isSolidTile(x+1,y) then
                       putTape()
                end
            end
        end

        -- react to inputs: move player
        if love.keyboard.isDown("left") and (player.tapeDir == nil or player.tapeDir == "left") then
            player.vx = player.vx - dt*50
        player.image = images.child_left
            if player.holding then
                player.holding.image = images.cat_left
            end
            if (love.keyboard.isDown("space")) and not player.taping and isSolid(player.x+TILESIZE, player.y) and not isSolid(player.x, player.y) then
                startTape()
                player.tapeDir = "left"
            end
        end
        if love.keyboard.isDown("right") and (player.tapeDir == nil or player.tapeDir == "right") then
            player.vx = player.vx + dt*50
        player.image = images.child_right
            if player.holding then
                player.holding.image = images.cat_right
            end
--old version:            if (love.keyboard.isDown("space") or love.keyboard.isDown(" ")) and not player.taping and isSolid(player.x-TILESIZE, player.y) and not isSolid(player.x, player.y) then
            if (love.keyboard.isDown("space")) and not player.taping and isSolid(player.x-TILESIZE, player.y) and not isSolid(player.x, player.y) then
                startTape()
                player.tapeDir = "right"
            end
        end
        if love.keyboard.isDown("up") and (player.tapeDir == nil or player.tapeDir == "up") then
            player.vy = player.vy - dt*50
        player.image = images.child_up
            if player.holding then
                player.holding.image = images.cat_up
            end
            if (love.keyboard.isDown("space")) and not player.taping and isSolid(player.x, player.y+TILESIZE) and not isSolid(player.x, player.y) then
                startTape()
                player.tapeDir = "up"
            end
        end
        if love.keyboard.isDown("down") and (player.tapeDir == nil or player.tapeDir == "down") then
            player.vy = player.vy + dt*50
        player.image = images.child_down
            if player.holding then
                player.holding.image = images.cat_down
            end
-- old version:           if (love.keyboard.isDown("space") or love.keyboard.isDown(" ")) and not player.taping and isSolid(player.x, player.y-TILESIZE) and not isSolid(player.x, player.y) then

            if (love.keyboard.isDown("space")) and not player.taping and isSolid(player.x, player.y-TILESIZE) and not isSolid(player.x, player.y) then
                startTape()
                player.tapeDir = "down"
            end
        end
        -- remove tape with Q key
        if love.keyboard.isDown("q") then
            x = math.floor(player.x/TILESIZE)
            y = math.floor(player.y/TILESIZE)
            if map[x][y].tape then
                sounds.rip:play()
                cutTape(x, y)
            end
        end

       -- check player for collision with walls and move player
        
	--set player collision points
        player.collide[1].x = player.x + PLAYERSIZE/2
	player.collide[1].y = player.y + PLAYERSIZE/2

        player.collide[2].x = player.x
	player.collide[2].y = player.y + PLAYERSIZE/2

        player.collide[3].x = player.x - PLAYERSIZE/2
	player.collide[3].y = player.y + PLAYERSIZE/2

        player.collide[4].x = player.x - PLAYERSIZE/2
	player.collide[4].y = player.y

        player.collide[5].x = player.x - PLAYERSIZE/2
	player.collide[5].y = player.y - PLAYERSIZE/2

        player.collide[6].x = player.x
	player.collide[6].y = player.y - PLAYERSIZE/2

        player.collide[7].x = player.x + PLAYERSIZE/2
	player.collide[7].y = player.y - PLAYERSIZE/2

        player.collide[8].x = player.x + PLAYERSIZE/2
	player.collide[8].y = player.y


        -- check player for collision with vertical wall/tape
        collides = false
            for i=1,#player.collide do
                if (isWall(player.collide[i].x + player.vx, player.collide[i].y) or (player.holding and isTape(player.collide[i].x + player.vx, player.collide[i].y))) then
                    collides = true
                end
            end

        -- move player in x direction
	if not collides then
            player.x = player.x + player.vx--*(dt*60)
        else
            player.vx = 0
	end


        -- check player for collision with horizontal wall/tape
        collides = false
            for i=1,#player.collide do
                if (isWall(player.collide[i].x, player.collide[i].y + player.vy) or (player.holding and isTape(player.collide[i].x, player.collide[i].y + player.vy))) then
                    collides = true
                end
            end

        -- move player in y direction
	if not collides then
            player.y = player.y + player.vy--*(dt*60)
        else
            player.vy = 0
	end

        -- apply friction to player movement
        player.vx = player.vx*0.8
        player.vy = player.vy*0.8


        -- set roomba movement
        if roomba.on then
            -- clean floor with roomba
            mx, my = roomba.x, roomba.y
            x = math.floor(mx/TILESIZE)
            y = math.floor(my/TILESIZE)
            b = ROOMBASIZE/TILESIZE -- calculate roomba size in tiles
            cx = (x-math.floor(b/2)) -- calculate upper left corner of the cleaned area
            cy = (y-math.floor(b/2))

            for xb=0,b-1 do
            	for yb=0,b-1 do
		    if not map[cx+xb][cy+yb].clean then --when new tiles have been cleaned
                        map[cx+xb][cy+yb].clean = true
		        checkWin()
                    end
            	end
            end

           -- check roomba for collision with walls and tapes (and cats)
            if roomba.vx > 0 then
                roomba.collide[1].x = roomba.x + ROOMBASIZE/2
                roomba.collide[2].x = roomba.x + ROOMBASIZE/2
                roomba.collide[3].x = roomba.x + ROOMBASIZE/2
            elseif roomba.vx == 0 then
                roomba.collide[1].x = roomba.x - ROOMBASIZE/2
                roomba.collide[2].x = roomba.x
                roomba.collide[3].x = roomba.x + ROOMBASIZE/2
            else
                roomba.collide[1].x = roomba.x - ROOMBASIZE/2
                roomba.collide[2].x = roomba.x - ROOMBASIZE/2
                roomba.collide[3].x = roomba.x - ROOMBASIZE/2
            end

            if roomba.vy > 0 then
                roomba.collide[1].y = roomba.y + ROOMBASIZE/2
                roomba.collide[2].y = roomba.y + ROOMBASIZE/2
                roomba.collide[3].y = roomba.y + ROOMBASIZE/2
            elseif roomba.vy == 0 then
                roomba.collide[1].y = roomba.y - ROOMBASIZE/2
                roomba.collide[2].y = roomba.y
                roomba.collide[3].y = roomba.y + ROOMBASIZE/2
            else
                roomba.collide[1].y = roomba.y - ROOMBASIZE/2
                roomba.collide[2].y = roomba.y - ROOMBASIZE/2
                roomba.collide[3].y = roomba.y - ROOMBASIZE/2
            end

            -- fail wip tape in case of collision with roomba
            for i=1,#roomba.collide do
                collide(roomba.collide[i].x + roomba.vx, roomba.collide[i].y + roomba.vy)
            end

            -- kill cats?
            for i,cat in ipairs(cats) do
                dist = math.sqrt(math.pow(cat.x-roomba.x,2) + math.pow(cat.y-roomba.y,2))
                if dist < 40 and not cat.held then
                    failLevel()
                end
            end

            -- random false positive collision detection so that roomba will move away from the walls
            random = love.math.random(1,150)
            if (random == 100 and roomba.vx ~= 0) then
                turnx = true
            else 
                turnx = false
            end
            if (random == 100 and roomba.vy ~= 0) then
                turny = true
            else
                turny = false
            end

            -- move roomba (forward if no collision, turn to the right if collision)

            collides = false
            for i=1,#roomba.collide do
                if isSolid(roomba.collide[i].x + roomba.vx, roomba.collide[i].y) then
                    collides = true
                end
            end

            if not collides and not turnx then
                roomba.x = roomba.x + roomba.vx--*(dt*60) -- go on in the same direction as before
            else
                if roomba.vx ~= 0 then
                    roomba.vy = roomba.vx
                    roomba.vx = 0
                end
            end

            collides = false
            for i=1,#roomba.collide do
                if isSolid(roomba.collide[i].x, roomba.collide[i].y + roomba.vy) then
                    collides = true
                end
            end

            if not collides and not turny then
                roomba.y = roomba.y + roomba.vy--*(dt*60)
            else
                if roomba.vy ~= 0 then
                    roomba.vx = -roomba.vy
                    roomba.vy = 0
                end
            end
        end

        --set roomba image depending on direction of movement
        if roomba.on then
            if roomba.vx > 0 then
            roomba.image = images.roomba_right
            elseif roomba.vx < 0 then
            roomba.image = images.roomba_left
            elseif roomba.vy > 0 then
            roomba.image = images.roomba_down
            else
            roomba.image = images.roomba_up
            end
        else
            if roomba.vx > 0 then
            roomba.image = images.roomba_off_right
            elseif roomba.vx < 0 then
            roomba.image = images.roomba_off_left
            elseif roomba.vy > 0 then
            roomba.image = images.roomba_off_down
            else
            roomba.image = images.roomba_off_up
            end
        end

        -- check cats for collision, reflect them if neccessary, and move them
        for i,cat in ipairs(cats) do
            -- for cats who are moving on their own:
            if not cat.held then
                if cat.vx == 1 then -- if cat moves to the right
                    cx = cat.x + CATSIZE/2 -- collision point on right side
                else
                    cx = cat.x - CATSIZE/2 -- collision point on left side
                end

                if cat.vy == 1 then -- if cat moves downwards
                    cy = cat.y + CATSIZE/2 -- collision point on bottom
                else
                    cy = cat.y - CATSIZE/2 -- collision point on top
                end

                -- fail wip tape in case of collision with cat
                collide(cx + cat.vx, cy + cat.vy)

		-- check if cat is colliding with wall/tape
                if isSolid(cx + cat.vx, cy + cat.vy) then --if cat is running into a solid
                    sounds.meow:setPitch(math.random(80, 150)/100)
                    sounds.meow:play()
                    -- on collision with horizontal solids
                    if isHorizontalCollision(cx, cy, cat.vx, cat.vy) then
                        --invert the vertical direction
                        cat.vy = -cat.vy
		        -- change image according to direction
                        if cat.vy == -1 then 
                            cat.image = images.cat_up
                        else
                            cat.image = images.cat_down
                        end
                    else --on collision with vertical solids, invert horizontal direction
                        cat.vx = -cat.vx
                        -- change image according to direction
                        if cat.vx == -1 then
                            cat.image = images.cat_left
                        else
                            cat.image = images.cat_right
                        end
                    end
                end

                cat.x = cat.x + cat.vx--*(dt*60)
                cat.y = cat.y + cat.vy--*(dt*60)
                -- frame rate adaption causes glitches that make cats go over walls
                -- therefore we disabled it for the time being

            else
                cat.x = player.x
                cat.y = player.y - 2.3*PLAYERSIZE
            end
        end
    end
end

-- react to keyboard inputs
function love.keypressed(key)

    -- quit game, pause or return to title screen with escape key
    if key == "escape" then
        if state == "title" or state == "end" then
            love.window.setFullscreen(false)
            love.event.quit()
        elseif state == "game" then
            pause()
        else
            level = 1
            state = "title"
        end
    elseif key == "f" then
        isFullscreen = love.window.getFullscreen()
        love.window.setFullscreen(not isFullscreen)
    -- cheat: skip this level with 0 key
    --elseif key == "0" then
    --    winLevel()
    -- go on to next screen with return key
    elseif key == "return" then
        if state == "title" then
            state = "introduction"
            text = "[insert text here]" -- haha  ;)
	elseif state == "pause" then
            resumeGame()
        elseif state == "introduction" then
            state = "game"
            loadLevel(level)
        elseif state == "end" then
            state = "game"
	    level = 1
            loadLevel(level)
        elseif state == "win" then
            state = "game"
            win = false
            level = level+1
            loadLevel(level)
        elseif state == "fail" then
            state = "game"
            loadLevel(level)
        end
    -- pick up/ set down cats and switch roomba on/off with E key
    elseif key == "e" then
        
   
            local x = math.floor(player.x/TILESIZE)
            local y = math.floor(player.y/TILESIZE)
            if map[x][y].tape then
                standingOnTape = true
	    else
                standingOnTape = false
            end

        if state == "game" and not (standingOnTape) then
        -- set down cat if player was holding a cat
            if player.holding then
                player.holding.x = player.x
                player.holding.y = player.y
		player.holding.held = false
                player.holding = nil
            else
         -- pick up the nearest cat if there is one near enough
                for i,cat in ipairs(cats) do
                    dist = math.sqrt(math.pow(cat.x-player.x,2) + math.pow(cat.y-player.y,2))
                    if dist < 50 and not player.holding then
                        cat.held = true
                        player.holding = cat
                        sounds.purr:play()
                    end
                end
        -- switch roomba on/off if it is near enough
                dist = math.sqrt(math.pow(roomba.x-player.x,2) + math.pow(roomba.y-player.y,2))
                if dist < 80 and not player.holding then
                    roomba.on = not roomba.on
                    if roomba.on then
                        music.roomba:play()
                    else
                        music.roomba:pause()
                    end
                end
            end
        end
    end
end

-- rip off the wip tape if space key is released before reaching the far end
function love.keyreleased(key)
    if state == "game" then
        if key == "space" then
            failTape()
        end
    end
end

function love.draw()
    love.graphics.setColor(255, 255, 255, 255)
    tlfres.beginRendering(CANVAS_WIDTH, CANVAS_HEIGHT)

    if state == "game" then
        love.graphics.push()
        love.graphics.translate(800, 450)
        love.graphics.scale(2)
        love.graphics.translate(-TILESIZE*(levels[level].lvlwidth/2+1), -TILESIZE*(levels[level].lvlheight/2+1))

        -- draw map
        for x=1,levels[level].lvlwidth do
            for y=1,levels[level].lvlheight do
                love.graphics.setColor(255,255,255)
                mapImg = images.dirty
                if map[x][y].clean then
                    --love.graphics.setColor(0,0,255)
                    mapImg = images.floor
                end
                if map[x][y].wall then
                    mapImg = images.wall
                    --love.graphics.setColor(255,255,255)
                end
                if map[x][y].wip then
                    mapImg = images.wip
                    --love.graphics.setColor(255,255,0)
                end
                if map[x][y].tape then
                    mapImg = images.tape
                    --love.graphics.setColor(100,100,100)
                end
                if tapeNode(x,y) then
                    --love.graphics.setColor(0,255,255)
                end

                love.graphics.draw(mapImg, TILESIZE*x, TILESIZE*y, 0, TILESIZE/8, TILESIZE/8)
            end
        end
        
        -- draw roomba
        love.graphics.draw(roomba.image, roomba.x, roomba.y, 0, 1, 1, roomba.image:getWidth()/2, roomba.image:getHeight()-(ROOMBASIZE/2))
        -- display roomba position point for debugging purposes: --

        --love.graphics.setColor(255, 0, 0)
        --love.graphics.setPointSize(5)
        --love.graphics.points(roomba.x, roomba.y)
        --love.graphics.setColor(255, 255,255)

        -- display roomba collision points for debugging purposes: --

        --for i=1,#roomba.collide do
        --    love.graphics.setColor(255, 255, 0)
        --    love.graphics.setPointSize(5)
        --    love.graphics.points(roomba.collide[i].x, roomba.collide[i].y)
        --end

        -- draw player 
        love.graphics.draw(player.image, player.x, player.y, 0, 1, 1, player.image:getWidth()/2, player.image:getHeight()-PLAYERSIZE/2)

        -- display player position point for debugging purposes: --

        --love.graphics.setColor(0, 255, 255)
        --love.graphics.setPointSize(5)
        --love.graphics.points(player.x, player.y)
        --love.graphics.setColor(255, 255,255)

        -- display player collision points for debugging purposes: --

        --for i=1,#player.collide do
        --  love.graphics.setColor(255, 255, 0)
        --  love.graphics.setPointSize(5)
        --  love.graphics.points(player.collide[i].x, player.collide[i].y)
        --end
        --love.graphics.setColor(255, 255,255)


        -- draw cats
        for i,cat in ipairs(cats) do
            love.graphics.setColor(cat.color)
            love.graphics.draw(cat.image, cat.x, cat.y, 0, 1, 1, cat.image:getWidth()/2, cat.image:getHeight()-(CATSIZE/2))
        -- display cat position point for debugging purposes: --

        --love.graphics.setColor(255, 0, 0)
        --love.graphics.setPointSize(5)
        --love.graphics.points(cat.x, cat.y)
        end
	love.graphics.setColor(255, 255, 255) -- set color back to white after drawing the cats

        love.graphics.pop()

        -- draw UI
    elseif state == "title" then
        love.graphics.draw(images.title, 0, 0, 0, 10, 10)
        instructions = startInstructions
    elseif state == "introduction" then
	-- display intro picture
        love.graphics.draw(images.introduction, 0, 0, 0, 5, 5)
	-- display intro text on the picture
	intro = "Spring cleaning time! Look at that dusty floor!\n\nMy roomba will help me, but it's too dangerous\n for my cat." .. 
	"\n\nGood thing I've got a lot\n of this barrier tape to protect it...\n\nIf this works, I might even be allowed to\n get more cats ~(^_^)~"
	love.graphics.print(intro, 600, 150)
        instructions = introductionInstructions
    else
        love.graphics.setFont(fonts.m5x7[100])
        love.graphics.printf(text, 100, 100, 1400, "center")
        love.graphics.setFont(fonts.m5x7[50])
    end
    -- display instructions text on the bottom of the screen
    love.graphics.printf(instructions, 0, 800, 1600, "center")

    -- end of the screen scaling part
    tlfres.endRendering()
end
-- check if a tile is a wall or tape tile (tile grid)
function isSolid(x, y)
    tx = math.floor(x/TILESIZE)
    ty = math.floor(y/TILESIZE)
    if tx < 1 or ty < 1 or tx > levels[level].lvlwidth or ty > levels[level].lvlheight then
        return false
    end
    return map[tx][ty].wall or map[tx][ty].tape
end
-- check if a tile is a wall tile (tile grid)
function isWall(x, y)
    tx = math.floor(x/TILESIZE)
    ty = math.floor(y/TILESIZE)
    if tx < 1 or ty < 1 or tx > levels[level].lvlwidth or ty > levels[level].lvlheight then
        return false
    end
    return map[tx][ty].wall
end
-- check if a tile is a tape tile (tile grid)
function isTape(x, y)
    tx = math.floor(x/TILESIZE)
    ty = math.floor(y/TILESIZE)
    if tx < 1 or ty < 1 or tx > levels[level].lvlwidth or ty > levels[level].lvlheight then
        return false
    end
    return map[tx][ty].tape
end
-- check if a tile is a tape tile (pixel grid)
function isTapeTile(x, y)
    if x < 1 or y < 1 or x > levels[level].lvlwidth or y > levels[level].lvlheight then
        return false
    end
    return map[x][y].tape
end

-- check if a tile is a wall or tape tile (pixel grid)
function isSolidTile(x, y)
    return map[x][y].wall or map[x][y].tape
end

-- find out if a collision is horizontal
-- input paramters: x,y position of moving object (in pixels)
--                  dx, dy direction of movement (in pixels)
function isHorizontalCollision(x, y, dx, dy) 
    tx = math.floor(x/TILESIZE)
    tx2 = math.floor((x+dx)/TILESIZE) -- calculate future position (x)
    ty = math.floor(y/TILESIZE)
    ty2 = math.floor((y+dy)/TILESIZE) -- calculate future position (y)

    return ty2 == ty + 1 or ty2 == ty - 1 -- returns true if movement was horiontal
end

-- attach a tape to the wall at the start
function startTape()
    sounds.tape_go:setPitch(math.random(70, 130)/100)
    sounds.tape_go:play()
    player.taping = true
    player.tapeStartX = math.floor(player.x/TILESIZE)
    player.tapeStartY = math.floor(player.y/TILESIZE)
    player.vy = 0
    player.vx = 0
end

-- attach a wip tape to the wall when reaching the far end
function putTape()
    sounds.tape_stop:setPitch(math.random(70, 130)/100)
    sounds.tape_stop:play()
    for x=1,levels[level].lvlwidth do
        for y=1,levels[level].lvlheight do
            if map[x][y].wip then
                map[x][y].wip = false
                map[x][y].tape = true
            end
        end
    end
    failTape()
end

-- rip off an unfinished tape when cancelled
function failTape()
    playRip = false
    for x=1,levels[level].lvlwidth do
        for y=1,levels[level].lvlheight do
            if map[x][y].wip then
                map[x][y].wip = false
                playRip = true
            end
        end
    end
    if playRip then
        sounds.rip:play()
    end
    player.taping = false
    player.tapeStartX = nil
    player.tapeStartY = nil
    player.tapeDir = nil
end

-- rip off unfinished tape when something collides with it
function collide(x, y)
    tx = math.floor(x/TILESIZE)
    ty = math.floor(y/TILESIZE)

    if tx < 1 or ty < 1 or tx > levels[level].lvlwidth or ty > levels[level].lvlheight then
        return false
    end

    if map[tx][ty].wip then
        failTape()
    end
end

-- find out if player is standing on a valid point to start drawing a tape
function validTapeStart(tx, ty)
    return not map[tx][ty].tape and (isSolidTile(tx-1, ty) or isSolidTile(tx+1, ty) or isSolidTile(tx, ty-1) or isSolidTile(tx, ty+1))
end

-- cut off the part of tape where player is standing
function cutTape(x, y)
    if x < 1 or y < 1 or x > levels[level].lvlwidth or y > levels[level].lvlheight then
        return false
    end

    local left = isTapeTile(x-1, y) and not tapeNode(x-1,y)
    local right = isTapeTile(x+1, y) and not tapeNode(x+1,y)
    local up = isTapeTile(x, y-1) and not tapeNode(x,y-1)
    local down = isTapeTile(x, y+1) and not tapeNode(x,y+1)

    map[x][y].tape = false

    if left then
        cutTape(x-1, y)
    end
    if right then
        cutTape(x+1, y)
    end
    if up then
        cutTape(x, y-1)
    end
    if down then
        cutTape(x, y+1)
    end
end

-- find out if a tape tile is a node where several tapes are connected
function tapeNode(x, y)
    tapeCount = 0
    if isTapeTile(x-1, y) then
        tapeCount = tapeCount+1
    end
    if isTapeTile(x+1, y) then
        tapeCount = tapeCount+1
    end
    if isTapeTile(x, y-1) then
        tapeCount = tapeCount+1
    end
    if isTapeTile(x, y+1) then
        tapeCount = tapeCount+1
    end

    return tapeCount > 2
end

-- show pause screen
function pause()
    state = "pause"
    text = "PAUSE"
    instructions = pauseInstructions
    music.roomba:pause()
end

-- resume playing after pause
function resumeGame()
    state = "game"
    instructions = gameInstructions
end

-- check if the condition for winning (floor clean) is reached
function checkWin()
    win = true
    for x=1,levels[level].lvlwidth do
        for y=1,levels[level].lvlheight do
            if not (map[x][y].clean or map[x][y].wall) then
                win = false
            end
        end
    end
    if win then
        winLevel()
    end
end

-- display message that player has won this level
function winLevel()
    state = "win"
    if levels[level].numberOfCats == 1 then
        text = "Yay, you did it! \\o/ Clean floor and happy cat!"
    else 
        text = "Yay, you did it! \\o/ Clean floor and " .. levels[level].numberOfCats .. " happy cats!"
    end

    text = text .. "\n\n" .. levels[level].story
    instructions = winInstructions
    music.roomba:pause()
    sounds.purr2:play()
end

-- display message that player has failed this level
function failLevel()
    state = "fail"
    text = "Oh no, poor kitty! :'("
    instructions = failInstructions
    music.roomba:pause()
    sounds.outch:play()
end
