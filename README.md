# Spring Clean - DO NOT CROSS

Made in 72 hours for *Ludum Dare 40* by blinry, dasMaichen, Flauschzelle, and winniehell.

Download the game from:

- https://ldjam.gitlab.io/spring-clean
- https://ldjam.gitlab.io/spring-clean/downloads

### License

- Code: [GPLv3+](https://www.gnu.org/licenses/gpl.html)
- Graphics, sound, and music: [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

We used the following sound effects from the Freesound Project:

- https://freesound.org/people/geodylabs/sounds/122820/
- https://freesound.org/people/TRNGLE/sounds/362652/
- https://freesound.org/people/Hamface/sounds/98669/
- https://freesound.org/people/AlaskaRobotics/sounds/245194/
- https://freesound.org/people/AlaskaRobotics/sounds/245196/
